let paragraphs = document.querySelectorAll("p");
paragraphs.forEach(paragraph => {
    paragraph.style.backgroundColor = "#ff0000";
});

let optionsListElement = document.getElementById("optionsList");
console.log(optionsListElement);

let parentElement = optionsListElement.parentElement;
console.log(parentElement);

let childNodes = optionsListElement.childNodes ;
childNodes.forEach(node => {
    console.log("Node name:", node.nodeName, ", Node Type:", node.nodeType);
});

let testParagraphElement = document.getElementById("testParagraph");
testParagraphElement.innerText = "This is a test paragraph";

let mainHeaderElements = document.getElementsByClassName("main-header");
for (let i = 0; i < mainHeaderElements.length; i++ ) {
    mainHeaderElements[i].classList.add("nav-item");
    let childElements = mainHeaderElements[i].querySelectorAll("*");
    childElements.forEach(element => {
        element.classList.add("nav-item");
    });
    console.log(childElements);
}

let sectionTitleElements = document.getElementsByClassName("section-title");
while (sectionTitleElements.length > 0) {
    sectionTitleElements[0].classList.remove("section-title");
}